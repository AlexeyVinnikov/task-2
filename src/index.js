const { initDB } = require("./database/config");
const RecordBook = require("./database/models/record-book.model");
const Record = require("./database/models/record.model");
const Teacher = require("./database/models/student.model");

async function bootstrap() {
  await initDB();
  const teacherLisr = await Teacher.findAll({
    include: [
      {
        model: Record,
        attribures: [],
        duplicates: false,
        include: [
          {
            model: RecordBook,
            attribures: [],
            required: true,
            where: {
              studentId: 1,
            },
          },
        ],
      },
    ],
  });

  console.log(teacherLisr);
}

bootstrap();
