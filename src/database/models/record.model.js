const { sequelize } = require("..");
const { Sequelize } = require("sequelize");

class Record extends Sequelize.Model {}

Record.init(
  {
    id: {
      type: Sequelize.DataTypes.UUID,
      defaultValue: Sequelize.DataTypes.UUIDV4,
      primaryKey: true,
    },
    subject: {
      type: Sequelize.STRING,
      allowNull: false,
    },
  },
  { sequelize: sequelize, modelName: "record" }
);

module.exports = Record;
