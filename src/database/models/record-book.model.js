const { sequelize } = require("..");
const { Sequelize } = require("sequelize");
const Record = require("./record.model");
const Teacher = require("./teacher.model");

class RecordBook extends Sequelize.Model {}

RecordBook.init(
  {
    id: {
      type: Sequelize.DataTypes.UUID,
      defaultValue: Sequelize.DataTypes.UUIDV4,
      primaryKey: true,
    },
    studentId: {
      type: Sequelize.STRING,
      allowNull: false,
    },
  },
  { sequelize: sequelize, modelName: "recordBook" }
);

RecordBook.HasMany(Record);

Record.belongsTo(RecordBook, {
  foreignKey: "recordBookId",
});

Teacher.HasMany(Record);

Record.belongsTo(Teacher, {
  foreignKey: "teacherId",
});

module.exports = RecordBook;
